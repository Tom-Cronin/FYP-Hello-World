package io.github.tom_cronin.fyphelloworld.dependant;

import io.github.tom_cronin.fyphelloworld.shared.ClassB;
import io.github.tom_cronin.fyphelloworld.shared.Multiplier;

public class HelloWorld {

    private final Multiplier multiplier;
    public HelloWorld(Multiplier multiplier, ClassB classB) {
        this.multiplier = multiplier;
    }

    public void printValue(){
        int value = this.multiplier.getMultiplier() * 2;
        System.out.println(value);
    }

    public static void main(String[] args) {
        new HelloWorld(new Multiplier(""), new ClassB());
    }
}
